﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Data.SqlServerCe;

namespace WinFormsAppTest
{
    public partial class UsuarioMaestro : Form
    {
        public UsuarioMaestro()
        {
            InitializeComponent();
        }

        private void UsuarioMaestro_Load(object sender, EventArgs e)
        {
            BindGrid();
        }

        private void BindGrid()
        {
            string connString = WinFormsAppTest.Properties.Settings.Default["TestAppWinFormsConnectionString"].ToString();
            using (SqlCeConnection con = new SqlCeConnection(connString))
            {
                using (SqlCeCommand cmd = new SqlCeCommand("SELECT * FROM SystemUser", con))
                {
                    try{
                        cmd.CommandType = CommandType.Text;
                        con.Open();
                        DataTable dt = new DataTable();
                        dt.Load(cmd.ExecuteReader());
                        dataGridView1.DataSource = dt;
                        con.Close();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                }
            }
        }
    }
}
