﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WinFormsAppTest
{
    public partial class Main : Form
    {
        public Main()
        {
            InitializeComponent();
        }

        private void Main_Load(object sender, EventArgs e)
        {

        }

        private void altasToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void consultaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CategoriaMaestro catMae = new CategoriaMaestro();
            catMae.ShowDialog();
        }

        private void altaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CategoriaDetalle catDet = new CategoriaDetalle();
            catDet.ShowDialog();
        }

        private void bajaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CategoriaDetalle catDet = new CategoriaDetalle();
            catDet.ShowDialog();
        }

        private void consultaToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            ProductoMaestro proMae = new ProductoMaestro();
            proMae.ShowDialog();
        }

        private void altaToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            ProductoDetalle proDet = new ProductoDetalle();
            proDet.ShowDialog();
        }

        private void bajaToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            ProductoDetalle proDet = new ProductoDetalle();
            proDet.ShowDialog();
        }

        private void consultaToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            UsuarioMaestro usrMae = new UsuarioMaestro();
            usrMae.ShowDialog();
        }

        private void altaToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            UsuarioDetalle usrDet = new UsuarioDetalle(3);
            usrDet.ShowDialog();
        }

        private void bajaToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            UsuarioDetalle usrDet = new UsuarioDetalle(4);
            usrDet.ShowDialog();
        }

        private void modificaciónToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            CategoriaDetalle catDet = new CategoriaDetalle();
            catDet.ShowDialog();
        }

        private void modificaciónToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            ProductoDetalle proDet = new ProductoDetalle();
            proDet.ShowDialog();
        }

        private void modificaciónToolStripMenuItem_Click(object sender, EventArgs e)
        {
            UsuarioDetalle usrDet = new UsuarioDetalle(5);
            usrDet.ShowDialog();
        }

    }
}
