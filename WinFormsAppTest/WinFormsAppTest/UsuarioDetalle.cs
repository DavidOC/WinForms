﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Data.OleDb;
using System.Configuration;
using System.Data.SqlServerCe;

namespace WinFormsAppTest
{
    public partial class UsuarioDetalle : Form
    {
        public int iAction { get; set; }
        public string connString;

        public UsuarioDetalle(int Action)
        {
            InitializeComponent();
            this.iAction = Action;
            connString = WinFormsAppTest.Properties.Settings.Default["TestAppWinFormsConnectionString"].ToString();

        }

        private void Aceptar_Click(object sender, EventArgs e)
        {
            if (iAction == 3) /*Alta*/
            {
                string message1 = "Se dio de alta al usuario satisfactoriamente";
                string caption1 = "Alta Usuario";
                MessageBoxButtons buttons1 = MessageBoxButtons.OK;
                DialogResult result1;

                AltaUsuario();

                result1 = MessageBox.Show(message1, caption1, buttons1);

                if (result1 == System.Windows.Forms.DialogResult.OK)
                {
                    // Closes the parent form
                    this.Close();
                }

            }

            if (iAction == 5) /*Modifica*/
            {
                string message1 = "Se modifico al usuario satisfactoriamente";
                string caption1 = "Modifica Usuario";
                MessageBoxButtons buttons1 = MessageBoxButtons.OK;
                DialogResult result1;

                ModificaUsuario();

                result1 = MessageBox.Show(message1, caption1, buttons1);

                if (result1 == System.Windows.Forms.DialogResult.OK)
                {
                    // Closes the parent form
                    this.Close();
                }

            }

            if (iAction == 4) /*Baja*/
            {
                string message = "Esta usted seguro de eliminar al usuario?";
                string caption = "Eliminar Usuario";
                MessageBoxButtons buttons = MessageBoxButtons.YesNo;
                DialogResult result;

                // Displays the MessageBox.

                result = MessageBox.Show(message, caption, buttons);

                if (result == System.Windows.Forms.DialogResult.Yes)
                {

                    // Closes the parent form.
                    eliminaUsuario();
                    this.Close();

                }
            }
        }

        private void UsuarioDetalle_Load(object sender, EventArgs e)
        {
            if (iAction == 3) /*Alta*/ 
            {
                btnAceptra.Text = "Aceptar";
            }
            if (iAction == 4) /*Baja*/
            {
                btnAceptra.Text = "Eliminar";
            }
            if (iAction == 5) /*Modificacion*/
            {
                btnAceptra.Text = "Aceptar";
            }
        }

        private void txtId_LostFocus(object sender, EventArgs e)
        {
            consultaUsuario(Int32.Parse(txtId.Text));
        }

        private void consultaUsuario(int IdUser)
        {
            using (SqlCeConnection con = new SqlCeConnection(connString))
            {
                using (SqlCeCommand cmd = new SqlCeCommand("SELECT id,userName,password,name,lastName FROM SystemUser where id =" + txtId.Text, con))
                {
                    try{
                        cmd.CommandType = CommandType.Text;
                        con.Open();
                        DataTable dt = new DataTable();
                        dt.Load(cmd.ExecuteReader());

                        txtUser.Text = dt.Rows[0][1].ToString();
                        txtPass.Text = dt.Rows[0][2].ToString();
                        txtName.Text = dt.Rows[0][3].ToString(); ;
                        txtApellido.Text = dt.Rows[0][4].ToString();

                        con.Close();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                }
            }
        }

        private void AltaUsuario()
        {
            string txtCommand = "INSERT INTO SystemUser (userName,password,name,lastName) values ('"
                                 + txtUser.Text + "','" + txtPass.Text + "','" + txtName.Text + "','"
                                 + txtApellido.Text + "')";

             using (SqlCeConnection con = new SqlCeConnection(connString))
            {
                using (SqlCeCommand cmd = new SqlCeCommand(txtCommand, con))
                {
                    try{
                        cmd.CommandType = CommandType.Text;
                        con.Open();
                        cmd.ExecuteNonQuery();
                        con.Close();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                }
            }
        }

        private void ModificaUsuario()
        {
            string txtCommand = "update SystemUser set userName ='" + txtUser.Text + "'" +
                                                      ",[password]='" + txtPass.Text + "'" +
                                                      ",name='" + txtName.Text + "'" +
                                                      ",lastName='" + txtApellido.Text + "'" +
                                "where id =" + txtId.Text;


            using (SqlCeConnection con = new SqlCeConnection(connString))
            {
                using (SqlCeCommand cmd = new SqlCeCommand(txtCommand, con))
                {
                    try
                    {
                        cmd.CommandType = CommandType.Text;
                        con.Open();
                        cmd.ExecuteNonQuery();
                        con.Close();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                }
            }
        }



        private void eliminaUsuario()
        {
            using (SqlCeConnection con = new SqlCeConnection(connString))
            {
                using (SqlCeCommand cmd = new SqlCeCommand("Delete FROM SystemUser where id =" + txtId.Text, con))
                {
                    try{
                        cmd.CommandType = CommandType.Text;
                        con.Open();
                        cmd.ExecuteNonQuery();
                        con.Close();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                }
            }
        }
    }
}


